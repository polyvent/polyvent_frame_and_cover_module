# PolyVent_Frame_and_cover_Module

This repo holds the design files for the Frame and Cover.

The current design if the "cake dome" designed for classroom instruction. The cover is literally tansparent and can be lifted off, allowing the system to be run with the cover attached or not.

The footprint in the design is rather large, being optimized for ease of acces in troubleshooting and ease of repair and change, rather than to economize space.

#DOI

[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.10642642.svg)](https://doi.org/10.5281/zenodo.10642642)
